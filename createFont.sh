#!/usr/bin/env bash

# A script to create type1 and type2 font image using ImageMagick
#
# Image optimisation done by pngquant.
# Website:More info http://pngquant.org/
# Source:https://github.com/pornel/pngquant 
#
# Copyright 2014-2016 blackosx
#
# Thanks to Alex for the giving me the idea and support to make it ;)

# set -x

Vers="0.88"

# ==================================================================
# Functions
# ==================================================================

StripValue()
{   
    local passedString="$1"
    local passedItemToGet="$2"
    local stripped="${passedString##*${passedItemToGet}: }"
    stripped="${stripped%%;*}"
    echo "$stripped"
}

GetMetrics()
{   
    local passedChar="$1"
    local Metrics=$( convert -debug annotate  xc: -font "$gFontToUse" -pointsize $gPointSize -annotate 0 "$passedChar" null: 2>&1 | grep Metrics: )
    echo "$Metrics"
}

Round()
{
    local passedValue="$1"
    local rounded=$( LC_ALL=C /usr/bin/printf "%0.f" "$passedValue" )
    echo "$rounded"
}
    
CalculateCharWidth()
{
    local passedChar="$1"
    local precedingCharSpace=0

    if [ "$passedChar" == "@" ]; then
        # The At char returns zero geometry width by itself.
        # So combine with another character, get the total width and then remove the width of the other character.
        local charBoundsRightXA=$(GetMetrics ".")       
        local charBoundsRightXA=$(StripValue "$charBoundsRightXA" "bounds")
        charBoundsRightXA="${charBoundsRightXA##* }"
        local charBoundsRightXARounded=$(Round "${charBoundsRightXA%,*}")
        
        local tmp=$(GetMetrics ".@")
        local charBoundsRightXB=$(StripValue "$tmp" "bounds")
        charBoundsRightXB="${charBoundsRightXB##* }"
        local charBoundsRightXBRounded=$(Round "${charBoundsRightXB%,*}")
        
        # Also check the Metric>Width incase it's larger.
        local charWidth=$(StripValue "$tmp" "width")
        local charWidthRounded=$(Round "$charWidth")
        
        if [ $charWidthRounded -gt $charBoundsRightXBRounded ]; then
            charWidthRounded=$(($charWidthRounded-$charBoundsRightXARounded))
        else
            charWidthRounded=$(($charBoundsRightXBRounded-$charBoundsRightXARounded))
        fi
    else
        # Does glyph exist? Compare to metrics of .?
        local tmpcharDebug=$(GetMetrics ".$passedChar")
        tmpcharDebug="${tmpcharDebug#*;}"
        if [ "$tmpcharDebug" != "$nonExistGlyphMetrics" ] || [ "$passedChar" == "?" ]; then

            local charDebug=$(GetMetrics "$passedChar")
            local charBounds=$(StripValue "$charDebug" "bounds")
        
            # Check for bounds width
            local charWidth="${charBounds##* }"
            local charWidthRoundedB=$(Round "${charWidth%,*}")
  
            # Also check if left most position is outside of bounds
            # If a value is negative then it needs adding to the left edge of char
            local leftEdgeBound="${charBounds%%,*}"

            if [ "${leftEdgeBound:0:1}" == "-" ]; then
                local valueToAdd=$( echo "$leftEdgeBound" | cut -c2- )
                precedingCharSpace=$(Round "${valueToAdd%,*}")
            fi
            
            # Added for type2 - does this affect type1? Don't think so.
            # Also check the Metric>Width incase it's larger.
            charWidth=$(StripValue "$charDebug" "width")
            local charWidthRounded=$(Round "$charWidth")
            [[ $charWidthRounded -le $charWidthRoundedB ]] && charWidthRounded=$charWidthRoundedB
        else
            charWidthRounded=0
            precedingCharSpace=0
        fi
    fi
    
    echo "${charWidthRounded}:${precedingCharSpace}"  # This line acts as a return to the caller.
}

GetTextHeight()
{
    local passedMetrics="$1"

    # Read bounds
    # Example:    bounds: 0.671875,18.1406 8.78125,30
    local tmpBounds=$(StripValue "$passedMetrics" "bounds")

    # Check for invalid value
    if [[ "$tmpBounds" == *e+* ]]; then
        tmpBounds=0
    else
        # Strip and round up the first value after the first comma
        tmpHeightBoundsBaseline="${tmpBounds#*,}"
        tmpHeightBoundsBaseline=$( echo "${tmpHeightBoundsBaseline% *}" | tr -d ' ' | sed 's/-//g' )
        tmpHeightBoundsBaseline=$(Round "$tmpHeightBoundsBaseline")

        # Strip and round up the second value after the comma
        tmpBounds=$(Round "${tmpBounds##*,}")

        # Add bounds baseline
        tmpBounds=$(($tmpBounds+$tmpHeightBoundsBaseline))

        # Add 2 pixels for slight gap
        tmpBounds=$(($tmpBounds+2))
    fi

    # Read height
    local tmpHeight=$(StripValue "$passedMetrics" "height")

    # Return value depending on font type requested
    if [ $gType -eq 1 ]; then
        if [ $tmpBounds -ne 0 ]; then
            local tmp=$tmpBounds
        else
            local tmp=$tmpHeight
        fi
    elif [ $gType -eq 2 ]; then
        # Compare height and bounds and use the largest value.
        if [ $tmpHeight -gt $tmpBounds ]; then
            local tmp=$tmpHeight
        else
            local tmp=$tmpBounds
        fi
    fi
    
    echo "$tmp:$tmpHeightBoundsBaseline"
}

displayHelp()
{
    echo "Usage: createFont.sh -f[path/to/font] -s[size] <options>"
    echo ""
    echo "Please supply a minimum of the font and size. In this case a PNG image"
    echo "will be generated with the text in white on a transparent background."
    echo ""
    echo "Options are:"
    echo "    -a                      Alpha transparency for text. Value 0 thru 1"
    echo "                            * Some versions of ImageMagick fail to render text"
    echo "                              with this option turned on."
    echo "    -b                      Background Colour. As three comma separated values"
    echo "                            Red, Green and Blue. [0-255],[0-255],[0-255]"
    echo "                            Note: background defaults to transparent."
    echo "    -c                      Colour of text. As three comma separated values"
    echo "                            Red, Green and Blue. [0-255],[0-255],[0-255]"
    echo "                            Note: text colour defaults to white."
    echo "                            Some colour examples:"
    echo "                            Red:           205,0,0"
    echo "                            Dark Blue:     35,43,148"
    echo "                            Light Blue:    93,192,244"
    echo "                            Bright Green:  91,226,81"
    echo "                            Dark Green:    24,135,16"
    echo "                            Bright Yellow: 219,221,47"
    echo ""
    echo "    -e                      Effects"
    echo "                            ----------------------------------------------------"
    echo "        shading             Enable single colour shading effect."
    echo "        shadow              Enable drop shadow effect (type 1 fonts only)."
    echo ""
    echo "    -f                      Font. Either installed font name OR full file path."
    echo "    -h                      Display help."
    echo "    -i                      List installed fonts available to ImageMagick."
    echo "    -l                      Leading (Space between text lines) +/-16 (type 2)"
    echo ""
    echo "    -r                      Reduced Character Set (Type 2 only)"
    echo "                            ----------------------------------------------------"
    echo "        a                   Do not include ANSI characters (32-127)"
    echo "        c                   Do not include cyrillic characters."
    echo ""
    echo "    -s                      Size in pixels. Expects value between 6 and 255"
    echo ""
    echo "    -t                      Type"
    echo "                            ----------------------------------------------------"
    echo "        1                   Font type for Chameleon (Default)."
    echo "        2                   Font type for Clover."
    echo ""
    echo "    -o                      Disable optimisation of final image using pngquant."
    echo "    -p                      Padding in pixels. Space between characters (type 1)"
    echo "    -v                      Display version of this script."   
    echo "    -x                      Disable anti aliasing."   
    echo ""
}

verifyColourArgument()
{
    local passedString="$1"

    # does value contain just numbers and a comma?
    if [[ "$OPTARG" =~ ^[-,0-9]+$ ]]; then
                
        declare -a rgb
        local oIFS="$IFS"; IFS=$','
        for o in $OPTARG
        do
            rgb+=($o)
        done
        IFS="$oIFS"

        # Have we three separate values?
        if [ "${#rgb[@]}" -ne 3 ]; then
            echo "Required colour values are [0-255],[0-255],[0-255]"
            echo "exiting"
            exit 1
        fi

        # Check each colour value for valid numbers
        if [[ ${rgb[0]} =~ ^-?[0-9]+$ ]] && [ ${rgb[0]} -ge 0 ] && [ ${rgb[0]} -le 255 ]; then
            gRed=${rgb[0]}
        else
            echo "Red value ${rgb[0]} is invalid. Needs to be 0-255"
            echo "exiting"
            exit 1
        fi
        if [[ ${rgb[1]} =~ ^-?[0-9]+$ ]] && [ ${rgb[1]} -ge 0 ] && [ ${rgb[1]} -le 255 ]; then
            gGreen=${rgb[1]}
        else
            echo "Green value ${rgb[1]} is invalid. Needs to be 0-255"
            echo "exiting"
            exit 1
        fi
        if [[ ${rgb[2]} =~ ^-?[0-9]+$ ]] && [ ${rgb[2]} -ge 0 ] && [ ${rgb[2]} -le 255 ]; then
            gBlue=${rgb[2]}
        else
            echo "Blue value ${rgb[2]} is invalid. Needs to be 0-255"
            echo "exiting"
            exit 1
        fi
    else
        echo "Required colour values are [0-255],[0-255],[0-255]"
        echo "exiting"
        exit 1
    fi
}

BuildType1()
{
    stringToCreate=('!' '\"' '#' '$' '%' '&' '’' '(' ')' '*' '+' ',' '-' '.' '/' '0' '1' '2' '3' '4' '5' '6' '7' '8' '9' ':' ';' '<' '=' '>' '?' '@' 'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I' 'J' 'K' 'L' 'M' 'N' 'O' 'P' 'Q' 'R' 'S' 'T' 'U' 'V' 'W' 'X' 'Y' 'Z' '[' '\\\' ']' '^' '_' '’' 'a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o' 'p' 'q' 'r' 's' 't' 'u' 'v' 'w' 'x' 'y' 'z' '{' '|' '}' '~' )
    # Strip all spaces
    tmpString=$(echo "${stringToCreate[@]}" | tr -d ' ')

    # Read the bounds font metric of the entire string to get height to allow better positioning of red dots.
    metricsOfString=$(GetMetrics "$tmpString")
    getValues=$(GetTextHeight "$metricsOfString")
    textHeight="${getValues%:*}"

    # Guestimate the canvas width by multiplying the number of chars in string by width of the W character
    returnValue=$(CalculateCharWidth "W")
    charWidth="${returnValue%:*}"  
    canvasWidth=$((${#stringToCreate[@]}*$charWidth))

    # Guestimate the canvas height by multiplying the width of the W character x 3
    canvasHeight=$(($charWidth*3))
    
    # Build Filename to save to
    paddedPointSize=$( LC_ALL=C /usr/bin/printf "%02d" "$gPointSize" )       # Pad as a string length of 2. For example: 06
    fontFileName="type${gType}_${fontName}_${paddedPointSize}"pt             # Filename to save font image.    
    
    if [ $padding -gt 0 ]; then
        # Add padding to filename
        fontFileName="$fontFileName"_p${padding}
        # Add extra width to guessed canvas to accommodate padding
        extraCanvasWidth=$((${#stringToCreate[@]}*$padding))
        canvasWidth=$(($canvasWidth+$extraCanvasWidth))
    fi

    # Initialise our Imagemagick command we will build.
    commandToBuildText="convert -size ${canvasWidth}x${canvasHeight} xc:\"$background\" -font \"$gFontToUse\" -pointsize $gPointSize"
    commandToBuildRedPixels="-fill \"$redPixel\""
    
    # Is Anti Aliasing to be disabled? (on by default)
    if [ $gUseAA -eq 0 ]; then
        commandToBuildText="${commandToBuildText} +antialias"
        fontFileName="$fontFileName"_noAA
    fi

    # Loop through array of characters and draw them to one image.
    # Also draw a single red pixel at correct positions to a second image.
    charWidthLast=0                                   # Width of the previous character.
    charPosX=0                                        # Starting position.
    redPixelPosX=0                                    # Starting position.
    echo "Building ImageMagick code sequence"
    for (( n=0; n<${#stringToCreate[@]}; n++ ))
    do
        # Find width of char
        returnValue=$(CalculateCharWidth "${stringToCreate[$n]}") 
        charWidth="${returnValue%:*}"  
        precedingCharSpace="${returnValue##*:}"

        # Adjust X position of character to include any negative left edge positioning.
        # Note - We don't adjust the position of the red pixel.
        charPosX=$(( $charPosX + $precedingCharSpace + $padding ))
    
        # Append further commands to our Imagemagick command
        # draw text character.
        commandToBuildText="$commandToBuildText -fill \"$textCol\" -draw \"text $charPosX,$textHeight '${stringToCreate[$n]}'\""
        # draw red pixel.
        commandToBuildRedPixels="$commandToBuildRedPixels -draw 'point $redPixelPosX,0'"
    
        # Work out the next character position
        charPosX=$(( charPosX + $charWidth ))
        redPixelPosX=$charPosX

        # Remember the width of the character we've just drawn
        charWidthLast=$(( charWidthLast + charWidth ))
        
        # Give user some feedback to progress
        printf "."
    done
    printf "\n"

    # Add final red pixel
    commandToBuildRedPixels="$commandToBuildRedPixels -draw 'point $charPosX,0'"

    # Add label and finish up
    length=$((${#stringToCreate[@]}*8)) # Don't yet understand the reason why this space is required for Label:
    spaces=$( printf "%${length}s" "" )

    # Finalise ImageMagick command (Add text, apply effect commands to the text, add red pixels, crop and give filename).
    commandToBuild="$commandToBuildText $effectCommandsPre $commandToBuildRedPixels label:\"${spaces}\" -trim $effectCommandsPost +repage \"${SELF_PATH%/*}/$fontFileName.png\""
}

BuildType2()
{
    declare -a chars
    #declare -a metricsWidth
    declare -a metricsBounds    # For comparing to ? when checking for existence of glyph
    declare -a metricsBoundsRight
    declare -a metricsBoundsLeft
    declare -a calculatedCharWidths
    declare -a rows
    charWidth=0
    
    # To help keep the char in the cell without touching the sides.
    textWidthAdjustment=2 
    textHeightAdjustment=2
    
    # Set all characters to be used (Includes ANSI and cyrillic characters).
    completeSetOfChars="!\"#$&’()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\\]^_’abcdefghijklmnopqrstuvwxyz{|}~ € ‚ƒ„…†‡ˆ‰Š‹Œ Ž  ‘’“”•–—˜™š›œ žŸ ¡¢£¤¥¦§¨©ª«¬ ®¯°±²³´µ¶·¸¹º»¼½¾¿АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя"
    # Read the bounds font metric of the entire string to get height
    metricsOfString=$(GetMetrics "$completeSetOfChars")
    getValues=$(GetTextHeight "$metricsOfString")
    textHeight="${getValues%:*}"
    textBaseline="${getValues#*:}"
    
    # Apply any user leading to textHeight
    textHeight=$(( $textHeight + $leading )) # This adds the leading above the final chars for each row
   
    chars=('.' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           ' ' '◎' '◉' '☐' '☑' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           ' ' '!' '\"' '#' '$' '%' '&' '’' '(' ')' '*' '+' ',' '-' '.' '/'\
           '0' '1' '2' '3' '4' '5' '6' '7' '8' '9' ':' ';' '<' '=' '>' '?'\
           '@' 'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I' 'J' 'K' 'L' 'M' 'N' 'O'\
           'P' 'Q' 'R' 'S' 'T' 'U' 'V' 'W' 'X' 'Y' 'Z' '[' '\\\' ']' '^' '_'\
           '’' 'a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o'\
           'p' 'q' 'r' 's' 't' 'u' 'v' 'w' 'x' 'y' 'z' '{' '|' '}' '~' ' '\
           )
           
    if [ $gIncludeAnsi -eq 1 ]; then
        chars+=('€' ' ' '‚' 'ƒ' '„' '…' '†' '‡' 'ˆ' '‰' 'Š' '‹' 'Œ' ' ' 'Ž' ' '\
           ' ' '‘' '’' '“' '”' '•' '–' '—' '˜' '™' 'š' '›' 'œ' ' ' 'ž' 'Ÿ'\
           ' ' '¡' '¢' '£' '¤' '¥' '¦' '§' '¨' '©' 'ª' '«' '¬' ' ' '®' '¯'\
           '°' '±' '²' '³' '´' 'µ' '¶' '·' '¸' '¹' 'º' '»' '¼' '½' '¾' '¿'\
           )
    else
        chars+=(' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           )
    fi
    
    if [ $gIncludeCyrillic -eq 1 ]; then
        chars+=('А' 'Б' 'В' 'Г' 'Д' 'Е' 'Ж' 'З' 'И' 'Й' 'К' 'Л' 'М' 'Н' 'О' 'П'\
           'Р' 'С' 'Т' 'У' 'Ф' 'Х' 'Ц' 'Ч' 'Ш' 'Щ' 'Ъ' 'Ы' 'Ь' 'Э' 'Ю' 'Я'\
           'а' 'б' 'в' 'г' 'д' 'е' 'ж' 'з' 'и' 'й' 'к' 'л' 'м' 'н' 'о' 'п'\
           'р' 'с' 'т' 'у' 'ф' 'х' 'ц' 'ч' 'ш' 'щ' 'ъ' 'ы' 'ь' 'э' 'ю' 'я'\
           )
    else
        chars+=(' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' ' '\
           )
    fi
    
    printf "Scanning font character metrics\n"
    printf "===============================\n"
        
    oIFS="$IFS"; IFS=$'/r' # Anything but a comma
    
    # Build metrics of each character
    for (( c=0; c<${#chars[@]}; c++ ))
    do
        currentChar="${chars[c]}"

        if [ "$currentChar" == "@" ]; then     # Metrics are not returned for @
            local tmp=$(GetMetrics ".@")       # Prepending with a period allows reading metrics
        elif [ "$currentChar" == " " ]; then   # Instead of processing a blank char
            local tmp=$(GetMetrics "!")        # Use exclamation mark
        else
            local tmp=$(GetMetrics "$currentChar")
        fi
        
        local tmpWidth=$(StripValue "$tmp" "width")
        local tmpWidthRounded=$(Round "${tmpWidth%,*}")
        #metricsWidth+=( $tmpWidthRounded )    

        local tmpBounds=$(StripValue "$tmp" "bounds")
        metricsBounds+=( $tmpBounds )
        local tmpBoundsRight="${tmpBounds##* }"
        local tmpBoundsRightRounded=$(Round "${tmpBoundsRight%,*}")
        metricsBoundsRight+=( $tmpBoundsRightRounded )

        local tmpBoundsLeft="${tmpBounds%%,*}"
        [[ "${tmpBoundsLeft:0:1}" == "-" ]] && tmpBoundsLeft=$( echo "$tmpBoundsLeft" | cut -c2- )
        local tmpBoundsLeftRounded=$(Round "${tmpBoundsLeft%,*}")
        metricsBoundsLeft+=( $tmpBoundsLeftRounded )

        local tmpBoundsWidth=$(( $tmpBoundsLeftRounded + $tmpBoundsRightRounded ))
        # Set final width to greater value
        [[ $tmpBoundsWidth -gt $tmpWidthRounded ]] && tmpWidthRounded=$tmpBoundsWidth
        
        # Need to record width of period.
        if [ "$currentChar" == "." ]; then
            periodWidthRounded=$tmpWidthRounded
        fi

        if [ "$currentChar" == "@" ]; then
            # Subtract period width from width of .@
            local currentCharWidth=$(( $tmpWidthRounded-$periodWidthRounded ))   
        else
            local currentCharWidth=$tmpWidthRounded  
        fi

        calculatedCharWidths+=( $currentCharWidth )
        
        # Give user some feedback of progress
        printf "."
    done
    printf "\n"
    
    IFS="$oIFS"
    
    # Debug - Print results
#    echo ".' | $periodWidthRounded | $periodBoundsLeftRounded | $periodBoundsLeftRounded"
#    echo "========================================="
#    for (( c=0; c<${#chars[@]}; c++ ))
#    do
#        echo "${chars[c]} | ${metricsWidth[c]} | ${metricsBoundsLeft[c]} | ${metricsBoundsRight[c]} | ${calculatedCharWidths[c]}"
#    done

    # Find widest character
    local widestIndex=0
    for (( c=0; c<${#calculatedCharWidths[@]}; c++ ))
    do
        [[ ${calculatedCharWidths[$c]} -gt $charWidth ]] && charWidth=${calculatedCharWidths[$c]} && widestIndex=$c
    done
    echo "Widest Char is at index $widestIndex | ${chars[$widestIndex]} at $charWidth pixels"
    
    # Add pixels to left and right edges to ensure character sits inside cell without touching edges.
    charWidth=$(( $charWidth + $textWidthAdjustment ))
    
    # Add pixels to top and bottom edges to ensure character sits inside cell without touching edges.
    textHeight=$(( $textHeight + $textHeightAdjustment ))

    # Calculate canvas size
    canvasWidth=$(( $charWidth * 16 ))
    canvasHeight=$(( $textHeight * 16 ))
    echo "Canvas size will be ${canvasWidth}x${canvasHeight} using cell size ${charWidth}x${textHeight}"
    
    # Initialise our ImageMagick command we will build.
    commandToBuildText="convert -size ${canvasWidth}x${canvasHeight} xc:\"$background\" -font \"$gFontToUse\" -pointsize $gPointSize -fill \"$textCol\""

    # Build Filename to save to.
    fontFileName="type${gType}_${fontName}_${gPointSize}pt_Cell_${charWidth}x${textHeight}_l${leading}"    # Filename to save font image.
    if [ $gIncludeAnsi -eq 0 ]; then
        if [ $gIncludeCyrillic -eq 0 ]; then
            fontFileName="${fontFileName}_rac"
        else
            fontFileName="${fontFileName}_ra"
        fi
    elif [ $gIncludeCyrillic -eq 0 ]; then
        fontFileName="${fontFileName}_rc"
    fi
    
    # Is Anti Aliasing to be disabled? (on by default)
    if [ $gUseAA -eq 0 ]; then
        commandToBuildText="${commandToBuildText} +antialias"
        fontFileName="$fontFileName"_noAA
    fi

    # When the final image is saved, the text is always a few pixels lower than wanted.
    # The attempted solution here is to use the baseline bounds metric from the font
    local topAdjustment=$textBaseline
    heightRepositionForCrop=$(( $textHeight - $topAdjustment - $textHeightAdjustment ))
    #echo "Will adjust vertical text position within canvas by $(( $topAdjustment - $textHeightAdjustment )) pixels"
    
    # Get metrics of a question mark as this is used when a glyph does not exist.
    local qmMetrics=$(GetMetrics "?")
    local qmBounds=$(StripValue "$qmMetrics" "bounds")  
    
    # Loop through array of characters and draw them to one image.
    echo "Building ImageMagick code sequence"

    # Build a 16x16 character grid.
    charCount=0
    for (( y=0; y<16; y++ ))
    do
        rowPosY=$(($textHeight * $y + $heightRepositionForCrop))
        cellStartX=0
        charPosX=0
        
        for (( x=1; x<17; x++ ))
        do
            thisCharWidth=${calculatedCharWidths[$charCount]}
            
            # Calculate Adjustment required to horizontally centre character in cell
            charXInCell=$(( ( $charWidth - $thisCharWidth ) / 2 )) 
            charPosX=$(( $cellStartX + $charXInCell ))
            
            # Shift across by any negative left bounds
            charPosX=$(( $charPosX + ${metricsBoundsLeft[$charCount]} ))

            # Append draw character commands to our ImageMagick command           
            # Check the metrics of each character don't match a question mark which is the
            # character ImageMagick assigns to a glyph which does not exist in the font.
            
            # Update: Some fonts do have valid chars with identical metrics to a question mark!
            
            # Most fonts will have alpha numeric characters so don't compare question mark metrics for those.
            
            #echo "${chars[$charCount]} | $qmBounds | ${metricsBounds[$charCount]} | ${metricsWidth[$charCount]}"
            
            # Let all AlphaNumeric chars through
            if [[ "${chars[$charCount]}" =~ ^[a-zA-Z0-9]+$ ]]; then
                commandToBuildText="$commandToBuildText -draw \"text $charPosX,$rowPosY '${chars[$charCount]}'\""
            else # Not alphanumeric char. Check metrics don't match question mark
                if [ "$qmBounds" != "${metricsBounds[$charCount]}" ] || [ "${chars[$charCount]}" == "?" ]; then 
                    commandToBuildText="$commandToBuildText -draw \"text $charPosX,$rowPosY '${chars[$charCount]}'\""
                else # Draw nothing as glyph does not exist (matches metrics of question mark)
                     commandToBuildText="$commandToBuildText -draw \"text $charPosX,$rowPosY ''\""   
                fi
            fi
            
            cellStartX=$(( $x * $charWidth ))
            ((charCount++))
        done
    done
    
    # Add label and finish up
    length=$((${#completeSetOfChars}*8)) # Don't yet understand the reason why this space is required for Label:
    spaces=$( printf "%${length}s" "" )
    
    # Finalise ImageMagick command (Add text, apply effect commands to the text and give filename).
    # Note the shadow effect on type2 only works if -trim is performed but that is not wanted ATM.
    # commandToBuild="$commandToBuildText $effectCommandsPre label:\"${spaces}\" $effectCommandsPost \"${SELF_PATH%/*}/$fontFileName.png\""
    
    # However, as there are no red pixels on a type2 image, we can run the shadow effect after it's been run.
    #commandToBuild="$commandToBuildText $effectCommandsPre label:\"${spaces}\" \"${SELF_PATH%/*}/$fontFileName.png\""
    
    # For now, shadow effects are not included for type 2 fonts.
    commandToBuild="$commandToBuildText $effectCommandsPre label:\"${spaces}\" \"${SELF_PATH%/*}/$fontFileName.png\""
}


# ==================================================================
# Main
# ==================================================================

# Check for ImageMagick Convert tool
CheckConvertTool=$( which convert )
if [ "$CheckConvertTool" == "" ] ; then
    echo "This script requires ImageMagick and it's convert tool to be installed."
    echo "The convert tool cannot be located."
    echo "Please install ImageMagick."
    echo "exiting."
    exit 1
fi

# Init some default vars which will can affected by user options.
gType=1
gFontToUse=""
gPointSize=0
gOptimise=1
gUseAA=1
gIncludeAnsi=1
gIncludeCyrillic=1
background="transparent"
bgColourRed=999
bgColourGreen=999
bgColourBlue=999
textColourRed=255
textColourGreen=255
textColourBlue=255
gUseAlphaText=0
textAlpha=1
padding=0
leading=0
applyEffect=""

# Check if any arguments were passed at all.
if [ $# -ne 0 ]; then 
    
    while getopts "a:b:c:e:f:hil:r:s:t:op:vx" opt; do
    case $opt in

        a) # ALPHA - Check the arguments
            
            gUseAlphaText=1
            
            # Is length 1 or 3 chars?
            if [ ${#OPTARG} -eq 1 ] || [ ${#OPTARG} -eq 3 ]; then
            
                if [ ${#OPTARG} -eq 1 ]; then
                    if [[ ${OPTARG} =~ [0-1] ]]; then
                        textAlpha=$OPTARG
                    else
                        echo "Value needs to be 0, 0.1 .. 0.9, or 1"
                        echo "exiting"
                        exit 1
                    fi
                fi
                
                if [ ${#OPTARG} -eq 3 ]; then
                    if [ ${OPTARG:0:1} == "0" ] && [ ${OPTARG:1:1} == "." ] && [[ ${OPTARG:2:1} =~ [0-9] ]]; then
                        textAlpha="$OPTARG"
                    else
                        echo "Value needs to be 0.1 to 0.9"
                        echo "exiting"
                        exit 1
                    fi
                fi
            else
                echo "Value needs to be 0, 0.1 .. 0.9, or 1"
                echo "exiting"
                exit 1
            fi
            ;; 
            
        b) # BACKGROUND - Check the arguments
            if [ ${#OPTARG} -ge 5 ] && [ ${#OPTARG} -le 11 ]; then
                verifyColourArgument "$OPTARG"
                bgColourRed=$gRed
                bgColourGreen=$gGreen
                bgColourBlue=$gBlue
            else
                echo "Required colour values are [0-255],[0-255],[0-255]"
                echo "exiting"
                exit 1
            fi
            ;;
            
        c) # COLOUR - Check the arguments
            if [ ${#OPTARG} -ge 5 ] && [ ${#OPTARG} -le 11 ]; then
                verifyColourArgument "$OPTARG"
                textColourRed=$gRed
                textColourGreen=$gGreen
                textColourBlue=$gBlue
            else
                echo "Required colour values are [0-255],[0-255],[0-255]"
                echo "exiting"
                exit 1
            fi
            ;;

        e) # EFFECT -  Check the arguments
            if [ ! $OPTARG == "shading" ] && [ ! $OPTARG == "shadow" ]; then
                echo "Effect option required is either shading or shadow"
                echo "exiting"
                exit 1
            else
                applyEffect="$OPTARG"
            fi
            ;;

        f) # FONT -  Check the arguments
            if [ -f "$OPTARG" ]; then
                # Arg is a file and it exists.
                gFontToUse=$OPTARG
            else
                # Is the arg a path or just a font name?
                fowardSlashCnt=$( grep -o "/" <<< "$OPTARG" | wc -l )
                if [ $fowardSlashCnt -le 1 ]; then
                    # Safe to guess it's a fontname.
                    # Is the font in ImageMagick's internal list?
                    fontMatch=$( convert -list font | grep "$OPTARG" )
                    if [ -n "$fontMatch" ]; then
                        gFontToUse=$OPTARG
                    else
                        echo "Font $OPTARG not found"
                        echo "exiting"
                        exit 1
                    fi
                else
                    echo "Font $OPTARG does not exist"
                    echo "exiting"
                    exit 1
                fi
            fi
            ;;

        h) # HELP
            displayHelp
            exit 1
            ;;
            
        i) # LIST INSTALLED FONTS
            convert -list font | grep Font:
            exit 1
            ;;
        
        l) # LEADING -  Check the arguments
            if [ $OPTARG -lt -16 ] || [ $OPTARG -gt 16 ]; then
                echo "Leading value is out of range. Use from +/-16"
                echo "exiting"
                exit 1
            else
                leading=$OPTARG
            fi
            ;;

        o) # DISBALE OPTIMISATION
            gOptimise=0
            ;;
        
        p) # PADDING -  Check the arguments
            if [ $OPTARG -lt 0 ] || [ $OPTARG -gt 255 ]; then
                echo "Value is out of range. Use from 0-255"
                echo "exiting"
                exit 1
            else
                padding=$OPTARG
            fi
            ;;
            
        r) # REDUCE font character set
            if [ $OPTARG == "a" ] || [ $OPTARG == "c" ] || [ $OPTARG == "ac" ]; then
                [[ $OPTARG == "a" ]] && gIncludeAnsi=0
                [[ $OPTARG == "c" ]] && gIncludeCyrillic=0
                [[ $OPTARG == "ac" ]] && gIncludeAnsi=0 && gIncludeCyrillic=0
            else
                echo "-r requires either a or c"
                echo "exiting"
                exit 1
            fi
            ;;
            
        s) # SIZE -  Check the arguments
            if [ $OPTARG -lt 6 ] || [ $OPTARG -gt 255 ]; then
                echo "Value is out of range. Use from 6-255"
                echo "exiting"
                exit 1
            else
                gPointSize=$OPTARG
            fi
            ;;

        t) # TYPE
            if [ $OPTARG -eq 1 ]; then
                gType=1
            elif [ $OPTARG -eq 2 ]; then
                gType=2
            fi
            ;;
            
        v) # VERSION
            echo "create_font_v$Vers"
            exit 1
            ;;
            
        x) # DISABLE AA
            gUseAA=0
            ;;
              
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
             
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
             
    esac
    done 
else
    echo "Create Font Script"
    echo "For creating font .PNG files for bootloaders."
    echo "Requires ImageMagick to be installed."
    echo ""
    displayHelp
    exit 1 
fi

# Check for minimum args
if [ "$gFontToUse" == "" ] || [ $gPointSize -eq 0 ]; then
    echo "Please supply a minimum of a font and a size."
    echo "exiting."
    exit 1
fi

# Init more vars
fontName="${gFontToUse##*/}"                      # Name of font specified by the user.
fontName="${fontName%%.*}"                        # Name of font with .extension removed.
textHeight=${gPointSize}                          # Set to pointsize but later check font's ascent Metric.
redPixel="RGB(255,0,0)"                           # Attributes for red pixel.
effectCommands=""                                 # For holding any requested effects.
precedingCharSpace=0                              # Adjustment for any char with negative left edge bounds.
imageMagickCommandFileName="IM_Command.command"   # Filename for imageMagick command.

# Set current DIR
SELF_PATH=$(cd -P -- "$(dirname -- "$0")" && pwd -P) && SELF_PATH=$SELF_PATH/$(basename -- "$0") 
RESOURCESDIR="${SELF_PATH%/*}/Resources"
pngquant="$RESOURCESDIR/pngquant"

# Set command file
imageMagickCommandFile="${SELF_PATH%/*}"/"$imageMagickCommandFileName"

# Set colour of the background
# These will be either supplied by user or otherwise defaults to transparent.
if [ $bgColourRed -ne 999 ] && [ $bgColourGreen -ne 999 ] && [ $bgColourBlue -ne 999 ]; then
    background="RGB($bgColourRed,$bgColourGreen,$bgColourBlue)"
fi
                
# Set colour and transparency of text.
# These will be either supplied by user or otherwise default values.
if [ $gUseAlphaText -eq 1 ]; then
    textCol="RGBA($textColourRed,$textColourGreen,$textColourBlue,$textAlpha)"
else
    textCol="RGB($textColourRed,$textColourGreen,$textColourBlue)"
fi

# Optional effects - this will be included in the buildType() Finalise Imagemagick command.
# See http://www.imagemagick.org/Usage/transform/#shade for more info
if [ ! $applyEffect == "" ] && [ $applyEffect == "shading" ]; then
    #effectCommands="" #-blur 0x3  # -shade 100x45
    #effectCommands="-alpha Extract -blur 0x6 -shade 120x21 -alpha On -normalize +level 15%  -fill Blue -tint 100%"
    effectCommandsPre="-alpha Extract -blur 0x1 -shade 120x21 -alpha On -normalize +level 15%  -fill \"$textCol\" -tint 100%"
elif [ ! $applyEffect == "" ] && [ $applyEffect == "shadow" ] && [ $gType -eq 1 ]; then
    effectCommandsPost="\( +clone -background \"${background}\" -shadow 80x3+0+4 \) +swap -background \"${background}\" -layers merge"
fi

# Build requested font type.
BuildType$gType 

# Save our built command to file
echo "$commandToBuild" > "$imageMagickCommandFile"
chmod 755 "$imageMagickCommandFile"

# Run command
echo "Running ImageMagick to create image"
"$imageMagickCommandFile"

if [ $gType -eq 1 ]; then
    # Add extra pixels for the space character to the left edge of Type1 fonts
    # Base the amount of space required on the width of the letter 't'.
    returnValue=$(CalculateCharWidth "t")
    charWidth="${returnValue%:*}"  
    echo "charWidth=$charWidth"
    echo "convert \"${SELF_PATH%/*}/${fontFileName}.png\" -background \"${background}\" -splice ${charWidth}x0 \"${SELF_PATH%/*}/${fontFileName}.png\"" > "${SELF_PATH%/*}"/IM_Splice_type1.command
    chmod 755 "${SELF_PATH%/*}"/IM_Splice_type1.command
    echo "Running ImageMagick to splice type 1 image"
    "${SELF_PATH%/*}"/IM_Splice_type1.command
fi

# Apply type2 shadow effect if requested
#if [ $gType -eq 2 ] && [ "$applyEffect" == "shadow" ]; then
#    cp "${SELF_PATH%/*}/${fontFileName}.png" "${SELF_PATH%/*}/${fontFileName}_before_shadow.png"
#    echo "Applying type2 font shadow effect"
#    echo "convert ${SELF_PATH%/*}/${fontFileName}.png \( +clone -background ${background} -shadow 80x3+0+4 \) +swap -background ${background} -layers merge ${SELF_PATH%/*}/${fontFileName}.png" > "$imageMagickCommandFile"
#    chmod 755 "$imageMagickCommandFile"
#    "$imageMagickCommandFile"
#fi

# Optimise and open final image file
if [ -f "${SELF_PATH%/*}/${fontFileName}.png" ]; then

    # Optimise image
    if [ $gOptimise -eq 1 ] && [ -f "$pngquant" ]; then
        #cp "${SELF_PATH%/*}/${fontFileName}.png" "${SELF_PATH%/*}/${fontFileName}_before_optimising.png"
        echo "Optimising ${SELF_PATH%/*}/$fontFileName.png"
        "$pngquant" --force --ext .png -- "${SELF_PATH%/*}/$fontFileName.png"
    fi
    
    # Open image
    echo "Opening ${SELF_PATH%/*}/${fontFileName}.png"  
    open "${SELF_PATH%/*}/${fontFileName}.png"
fi

# clean up
if [ -f "$imageMagickCommandFile" ]; then
    rm "$imageMagickCommandFile"
fi

if [ $gType -eq 1 ] && [ -f "${SELF_PATH%/*}"/IM_Splice_type1.command ]; then
    rm "${SELF_PATH%/*}"/IM_Splice_type1.command
fi