# README #

A bash script for generating .PNG image of a typeface in two predetermined structures.

This repository also includes a pngquant binary which the script uses to optimise the PNG image.

Website: https://pngquant.org

Source: https://github.com/pornel/pngquant

### Requirements ###

Imagemagick

http://www.imagemagick.org/script/index.php 


### Usage ###


```
#!shell

./createFont.sh -f[path/to/font] -s[size] <options>
```


Options are:
```
#!shell

Please supply a minimum of the font and size. In this case a PNG image
will be generated with the text in white on a transparent background.
 
Options are:
    -a                      Alpha transparency for text. Value 0 thru 1
                            * Some versions of ImageMagick fail to render text
                              with this option turned on.
    -b                      Background Colour. As three comma separated values
                            Red, Green and Blue. [0-255],[0-255],[0-255]
                            Note: background defaults to transparent.
    -c                      Colour of text. As three comma separated values
                            Red, Green and Blue. [0-255],[0-255],[0-255]
                            Note: text colour defaults to white.
                            Some colour examples:
                            Red:           205,0,0
                            Dark Blue:     35,43,148
                            Light Blue:    93,192,244
                            Bright Green:  91,226,81
                            Dark Green:    24,135,16
                            Bright Yellow: 219,221,47
 
    -e                      Effects
                            ----------------------------------------------------
        shading             Enable single colour shading effect.
        shadow              Enable drop shadow effect (type 1 fonts only).
 
    -f                      Font. Either installed font name OR full file path.
    -h                      Display help.
    -i                      List installed fonts available to ImageMagick.
    -l                      Leading (Space between text lines) +/-16 (type 2)
 
    -r                      Reduced Character Set (Type 2 only)
                            ----------------------------------------------------
        a                   Do not include ANSI characters (32-127)
        c                   Do not include cyrillic characters.
 
    -s                      Size in pixels. Expects value between 6 and 255
 
    -t                      Type
                            ----------------------------------------------------
        1                   Font type for Chameleon (Default).
        2                   Font type for Clover.
 
    -o                      Disable optimisation of final image using pngquant.
    -p                      Padding in pixels. Space between characters (type 1)
    -v                      Display version of this script.
    -x                      Disable anti aliasing.
```